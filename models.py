from google.cloud import datastore
from settings import *


class Task():
	def __init__(self, client, entity):
		self.client = client
		self.entity = entity

	@property
	def task(self):
		return self.entity

	def update(self, data):
		self.entity.update(data)
		self.client.put(self.entity)

	def delete(self):
		self.client.delete(self.entity.key)


class Tasks():
	def __init__(self):
		self.client = datastore.Client()
		self.kind = DATASTORE_KIND

	def get_all_tasks(self):
		query = self.client.query(kind=self.kind)
		result = list( query.fetch() )
		return [
			{
				'id': entity.id,
				'complete': entity['complete'],
				'title': entity['title'],
			} for entity in result
		]

	def get_task(self, task_id):
		try:
			task_id = int(task_id)
		except ValueError:
			return None
		key = self.client.key(self.kind, task_id)
		result = Task(self.client, self.client.get(key))
		return result

	def put_task(self, title):
		key = self.client.key(self.kind)
		entity = datastore.Entity(key=key)
		entity.update({
			'complete': False,
			'title': title,
		})
		self.client.put(entity)
		return entity
