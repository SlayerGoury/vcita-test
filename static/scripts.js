function callAjax(url, method, callback, fallback) {
	var xmlhttp;
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				callback(xmlhttp.responseText);
			} else {
				fallback(xmlhttp.responseText);
			}
		}
	}
	xmlhttp.open(method, url, true);
	xmlhttp.send();
}

function updateTasks() {

	function taskBuilder(data) {
		target = document.getElementById('tasks');
		tasks = JSON.parse(data);
		target.innerHTML = '';
		for (i=0; i<tasks.length; i++) {
			target.appendChild( taskElement( tasks[i] ) );
		}
	}

	function taskElement(data) {
		task = document.createElement('div');
		task.id = 'task-'+data['id'];

		checkbox = document.createElement('div');
		checkbox.classList.add('status');
		checkbox.classList.add(data['complete'] ? 'checked' : 'unchecked');
		checkbox.addEventListener('click', taskUpdater, false);

		title = document.createElement('p');
		title.innerText = data['title'];

		deleter = document.createElement('div');
		deleter.classList.add('deleter');
		deleter.addEventListener('click', taskDeleter, false);

		task.appendChild(checkbox);
		task.appendChild(title);
		task.appendChild(deleter);
		return task;
	}

	function taskUpdater(event) {
		function cb(data) {
			// FIXME handle multiple instances for fast clickers
			checkbox.classList.remove('progress')
			if (data == '1') { checkbox.classList.add('checked'); }
			else if (data == '0') { checkbox.classList.add('unchecked'); }
			else { checkbox.classList.add('progress'); }
		}
		checkbox = event.target;
		if (checkbox.classList.contains('checked')) { action = 'uncheck' }
		else if (checkbox.classList.contains('unchecked')) { action = 'check' }
		else { action = 'none' }
		checkbox.classList.remove('checked', 'unchecked');
		checkbox.classList.add('progress');
		task_id = checkbox.parentElement.id.replace('task-', '');
		callAjax('/tasks/'+task_id+'/'+action, 'PATCH', cb, reconnector(false));
	}

	function taskDeleter(event) {
		function cb(data) {
			event.target.parentElement.remove();
		}
		event.target.classList.add('progress');
		task_id = event.target.parentElement.id.replace('task-', '');
		callAjax('/tasks/'+task_id, 'DELETE', cb, reconnector(false));
	}

	callAjax('/tasks', 'GET', taskBuilder, reconnector(updateTasks));
}


function reconnector(target) {
	function rc() {
		// TODO handle connection problems here
	}
	return rc;
}


function toggleAdder() {
	placeholder = document.getElementById('adder_placeholder');
	adder = document.getElementById('adder');
	title = document.getElementById('task_title');
	if (placeholder.classList.contains('hidden')) {
		placeholder.classList.remove('hidden');
		adder.classList.add('hidden');
	} else {
		placeholder.classList.add('hidden');
		adder.classList.remove('hidden');
		title.select();
		title.focus();
	}
}

function addTask() {
	function cb(data) {
		// TODO add a progress indicator
		if (data) { updateTasks() }
	}
	title = document.getElementById('task_title').value;
	if (title) {
		// TODO encode title properly
		callAjax('/tasks/new/'+title, 'PUT', cb, reconnector(addTask));
	}
}
