import json
import os
import sys
import unittest
from application import app
from models import Tasks


class LackingTestCredentialsError(BaseException):
	""" GOOGLE_TEST_CREDENTIALS variable is not set """
	pass


class ApplicationTests(unittest.TestCase):
	def setUp(self):
		test_credentials = os.environ.get('GOOGLE_TEST_CREDENTIALS')
		if not test_credentials:
			raise LackingTestCredentialsError

		backup_credentials = os.environ.get('GOOGLE_APPLICATION_CREDENTIALS_BACKUP')
		if backup_credentials:
			self.oldenv = backup_credentials
		else:
			self.oldenv = os.environ.get('GOOGLE_APPLICATION_CREDENTIALS')
			os.environ['GOOGLE_APPLICATION_CREDENTIALS_BACKUP'] = self.oldenv

		os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = test_credentials
		app.config['TESTING'] = True
		app.config['DEBUG'] = False
		self.app = app.test_client()
		self.model = Tasks()
		tasks = self.model.get_all_tasks()
		for task in tasks:
			instance = self.model.get_task(task['id'])
			instance.delete()

	def tearDown(self):
		os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = self.oldenv
		os.environ['GOOGLE_APPLICATION_CREDENTIALS_BACKUP'] = ''

	def helper_put_task(self):
		self.app.put('/tasks/new/meow')
		response = self.app.get('/tasks')
		data = json.loads(response.data)
		task = next(task for task in data if task['title'] == 'meow')
		return task

	def helper_reload_task(self, old_task):
		response = self.app.get('/tasks')
		data = json.loads(response.data)
		new_task = next(task for task in data if task['title'] == old_task['title'])
		return new_task

	def test_index(self):
		response = self.app.get('/')
		self.assertEqual(response.status_code, 200)                                    # page should be served

	def test_tasks(self):
		response = self.app.get('/tasks')
		self.assertEqual(response.status_code, 200)                                    # task list should be served
		self.assertIn(b'[]', response.data)                                            # task list should be empty because it was emptied in setUp

	def test_put_task(self):
		response = self.app.put('/tasks/new/meow')
		self.assertEqual(response.status_code, 200)                                    # PUT request should be accepted
		self.assertIn(b'meow', response.data)                                          # should return task data
		response = self.app.get('/tasks')
		self.assertIn(b'meow', response.data)                                          # should also return task data when requesting entire task list
		self.assertIn(b'"complete": false', response.data)                             # new task should not be marked as complete

	def test_patch_task(self):
		task = self.helper_put_task()
		response = self.app.patch('/tasks/{}/{}'.format(task['id'], 'check'))
		self.assertEqual(response.status_code, 200)                                    # PATCH request should be accepted
		task = self.helper_reload_task(task)
		self.assertTrue(task['complete'])                                              # task should now be marked as complete
		response = self.app.patch('/tasks/{}/{}'.format(task['id'], 'uncheck'))
		self.assertEqual(response.status_code, 200)                                    # PATCH request should be accepted
		task = self.helper_reload_task(task)
		self.assertFalse(task['complete'])                                             # task should no longer be marked as complete

	def test_delete_task(self):
		task = self.helper_put_task()
		response = self.app.delete('/tasks/{}'.format(task['id']))
		self.assertEqual(response.status_code, 200)                                    # DELETE request should be accepted
		response = self.app.get('/tasks')
		self.assertIn(b'[]', response.data)                                            # task should no longer be present in task list

	def test_patch_task_with_wrong_action(self):
		task = self.helper_put_task()
		response = self.app.patch('/tasks/{}/{}'.format(task['id'], 'purr'))
		self.assertEqual(response.status_code, 204)                                    # PATCH request should be accepted but nothing should be returned in response

	def test_patch_task_with_wrong_id(self):
		response = self.app.patch('/tasks/{}/{}'.format('oh%20no', 'check'))
		self.assertEqual(response.status_code, 404)                                    # PATCH request should be accepted, 404 response should be returned

	def test_delete_task_with_wrong_id(self):
		response = self.app.delete('/tasks/{}'.format('oh%20no'))
		self.assertEqual(response.status_code, 404)                                    # DELETE request should be accepted, 404 response should be returned

	def test_model_is_empty(self):
		self.assertEqual(self.model.get_all_tasks(), [])                               # returned list should be empty because model was emptied in setUp

	def test_model_can_put_and_get(self):
		entity = self.model.put_task('meow')
		task = self.model.get_task(entity.id)
		self.assertEqual(entity, task.task)                                            # what we put is what we should get

	def test_model_new_task_is_not_complete(self):
		entity = self.model.put_task('meow')
		task = self.model.get_task(entity.id)
		self.assertFalse(task.task['complete'])                                        # new task should not be complete by default

	def test_model_new_task_title(self):
		entity = self.model.put_task('meow')
		task = self.model.get_task(entity.id)
		self.assertEqual(task.task['title'], 'meow')                                   # title should be what we put into model

	def test_model_can_not_get_nonexistent(self):
		task = self.model.get_task('oh%20no')
		self.assertIsNone(task)                                                        # bad id should result in None return
		task = self.model.get_task('42')
		self.assertIsNone(task.task)                                                   # nonexistend but otherwise good id should result in task object with None entity attached

	def test_model_update(self):
		entity = self.model.put_task('meow')
		task = self.model.get_task(entity.id)
		task.update({'complete': True})
		task = self.model.get_task(entity.id)
		self.assertTrue(task.task['complete'])                                         # Trueing completion should result in completion returned as True
		task.update({'title': 'purr'})
		self.assertEqual(task.task['title'], 'purr')                                   # Updating title should result in title being updated

	def test_model_delete(self):
		entity = self.model.put_task('meow')
		task = self.model.get_task(entity.id)
		task.delete()
		task = self.model.get_task(entity.id)
		self.assertIsNone(task.task)                                                   # instance with this id should no longer exist because it was deleted

if __name__ == "__main__":
	unittest.main()
