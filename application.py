from flask import Flask, render_template, request
from google.cloud import datastore
import json
from models import Tasks

app = Flask(__name__, template_folder='templates')
model = Tasks()


@app.route('/')
def index():
	return render_template('index.html')


@app.route('/tasks', methods=['GET'])
def tasks():
	result = model.get_all_tasks()
	return json.dumps(result)


@app.route('/tasks/<task_id>/<action>', methods=['PATCH'])
def patch_task(task_id, action=None):
	entity = model.get_task(task_id)
	if not entity:
		return 'bad id', 404

	if action == 'check':
		entity.update({'complete': True})
		return '1'
	elif action == 'uncheck':
		entity.update({'complete': False})
		return '0'
	else:
		return '', 204

@app.route('/tasks/<task_id>', methods=['DELETE'])
def delete_task(task_id):
	entity = model.get_task(task_id)
	if not entity:
		return 'bad id', 404
	entity.delete()
	return '1'


@app.route('/tasks/new/<title>', methods=['PUT'])
def put_task(title):
	entity = model.put_task(title)
	return entity


if __name__ == '__main__':
	app.run(host='127.0.0.1', port=8080, debug=False)
