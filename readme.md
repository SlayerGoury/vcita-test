# Deployment

Simply `gcloud app deploy` to deploy this thing to your gcloud

If you want to run it locally, set up your python environment and do  
`pip install -r requirements.txt`  
`export GOOGLE_APPLICATION_CREDENTIALS="/full/path/to/your/gcloud/credentials/file.json"`  
`export FLASK_APP=application.py`  
`flask run`

# Testing

Set up or activate testing datastore instance.

**WARNING**: do not use production instance for testing!  
Tests do empty the database.  
Also use datastore emulator to save money.

Activate your environment with requirements installed and do  
`export GOOGLE_TEST_CREDENTIALS="/testing/credentials/file.json"`  
`python test_application.py`

## Coverage testing

Coverage testing also runs all the tests, so there's no need to run  
`python test_application.py`  
in this case.

You'll need to  
`pip install -r requirements_test.txt`  
before you begin, but only once.

Once everything is installed, run  
`coverage run test_application.py`  
`coverage report *.py`

If you want to have some cute HTML reports, do  
`coverage HTML *.py`  
and then open `htmlcov/index.html` in a browser
